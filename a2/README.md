> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Assignment 2 Requirements:

1. Backward Engineer Payroll Calculator screenshots using Python
2. Organize program into main.py and functions.py
3. Provide screenshots of results
4. Create *a2_payroll_calculator* Jupyter Notebook


#### README.md file should include the following items:

* Screenshot of a2_payroll_calculator application running with and without overtime
* Screenshot of A2 Jupyter Notebook .ipynb file
* Screenshots of Skillsets 1, 2, and 3


#### Assignment Screenshots:

*Payroll Calculator with no overtime*:

![Payroll Calculator no overtime](img/Payroll_no_overtime.png)

*Payroll Calculator with overtime*:

![Payroll Calculator with overtime](img/Payroll_with_overtime.png)

*A2 Jupyter Notebook functions .ipynb*:

![Assignment 2 Jupyter Notebook](img/a2_jupyter_1.png)

*A2 Jupyter Notebook main .ipynb*:

![Assignment 2 Jupyter Notebook](img/a2_jupyter_2.png)

*Skillset 1: Square Feet to Acres Screenshot*:

![SS1 Square Feet to Acres Screenshot](img/SS1.png)

*Skillset 2: Miles per Gallon Screenshot*:

![SS2 Miles per Gallon Screenshot](img/SS2.png)

*Skillset 3: IT Student Percentage Screenshot*:

![SS3 IT Student Percentage](img/SS3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")
