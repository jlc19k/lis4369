> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Project 2 Requirements:

1. Backward-engineer project requirements to create [lis4369_p2.R](R_Project_2/lis4369_p2.R "lis4369_p2.R")
2. Run lis4369_p2.R and include required screenshots

#### README.md file should include the following items:

* At least one 4-panel RStudio screenshot executing [lis4369_p2.R](R_Project_2/lis4369_p2.R "lis4369_p2.R")
* Screenshots of at least two plots (qplot and plot)

#### Assignment Screenshots:

*4-panel RStudio screenshot for lis4369_p2.R*:

![lis4369_p2.R](img/p2_4panel.png)

*Screenshots of two plots*:

![Graph 1](img/p2_plot_1.png)  
![Graph 2](img/p2_plot_2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
