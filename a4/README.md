> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Assignment 4 Requirements:

1. Backward Engineer *demo.py* and *demo2.py* to create *a4_data_analysis_2*
2. Ensure all necessary packages are installed correctly
3. Create at least three functions: main(), get_requirements(), and data_analysis_2()
4. Create *a4_data_analysis_2* Jupyter Notebook
5. Complete skillsets 10, 11, and 12

#### README.md file should include the following items:

* Screenshots of a4_data_analysis_2 results in VSCode, including required graph
* Screenshots of A4 Jupyter Notebook .ipynb file
* Screenshots of Skillsets 10, 11, and 12


#### Assignment Screenshots:

*Screenshots of a4_data_analysis_2 results in VSCode*:

| ![A4 results part 1](img/a4_results_1.png) | ![A4 results part 2](img/a4_results_2.png) |

*Screenshot of A4 Data Analysis 2 graph*:

![A4 Data Analysis 2 graph](img/a4_graph.png)

| *Screenshot of A4 Jupyter Notebook*: | *Screenshot of A4 Jupyter Notebook Results*: |
| --- | --- |
| ![A4 Jupyter Notebook](img/a4_jupyter_1.png) | ![A4 Jupyter Notebook Results](img/a4_jupyter_2.png) |

*Screenshot of A4 Jupyter Notebook graph*:

![A4 Jupyter Notebook graph](img/a4_jupyter_3.png)

| *Skillset 10*: | *Skillset 11*: | *Skillset 12*: |
| --- | --- | --- |
| ![Skillset 10](img/ss10.png) | ![Skillset 11](img/ss11.png) | ![Skillset 12](img/ss12.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
