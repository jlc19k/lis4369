> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Project 1 Requirements:

1. Backward Engineer *demo.py* to create *p1_data_analysys_1*
2. Install pandas, pandas-datareader, and matplotlib
3. Create at least three functions: main(), get_requirements(), and data_analysis_1()
4. Create *P1 Data Analysis 1* Jupyter Notebook
5. Complete skillsets 7, 8, and 9

#### README.md file should include the following items:

* Screenshot of p1_data_analysis_1.py running in Visual Studio Code
* Screenshot of P1 Data Analysis 1 Jupyter Notebook
* Screenshots of skillsets 7, 8, and 9

#### Assignment Screenshots:

*p1_data_analysis_1 Visual Studio Code*:

![P1 VSCODE](img/p1_vscode_1.png)

*p1_data_analysis_1 data visualization in Visual Studio Code*:

![P1 Results VSCODE](img/p1_vscode_2.png)

| *Screenshot of P1 Jupyter Notebook*: | *Screenshot of P1 Jupyter Notebook Results*: |
| --- | --- |
| ![P1 Jupyter Notebook](img/p1_Jupyter_1.png) | ![P1 Jupyter Notebook Results](img/p1_Jupyter_2.png) |

#### Skillsets:

| *Skillset 7*: | *Skillset 8*: | *Skillset 9*: |
| --- | --- | --- |
| ![Skillset 7](img/ss7.png) | ![Skillset 8](img/ss8.png) | ![Skillset 9](img/ss9.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
