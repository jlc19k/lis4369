import datetime as dt
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def main():
    get_requirements()
    data_analysis_1()

def get_requirements():
    print("Data Analysis 1")
    print("\nProgram Requirements:")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing installations.")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations:")
    print("\ta. pandas (only if missing)")
    print("\tb. pandas-datareader (only if missing)")
    print("\tc. matplotlib (only if missing)")
    print("5. Create at least 3 functions that are called by the program:")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays the program requirements.")
    print("\tc. data_analysis_1(): displays the following data.")

def data_analysis_1():

    start = dt.datetime(2010, 1, 1)
    end = dt.datetime.now()

    df = pdr.DataReader(["DJIA", "SP500"], "fred", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns:")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines:")
    print(df.head(5))

    print("\nPrint last five lines:")
    print(df.tail(5))

    print("\nPrint first 2 lines:")
    print(df.head(2))

    print("\nPirst last 2 lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['DJIA'].plot()
    df['SP500'].plot()
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()