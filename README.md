# LIS4369 - Extensible Enterprise Solutions 
# (Python / R Data Analytics / Visualization)

## Jason Choate

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1 tip calculator* Jupyter Notebook
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Backward Engineer *a2_payroll_calculator* using Python
    - Organize program into main.py and functions.py
    - Provide screenshots of results
    - Create *a2_payroll_calculator* Jupyter Notebook

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Backward Engineer *a3_painting_estimator
    - Organize program into main.py and functions.py
    - Provide screenshots of results
    - Create *a3_painting_estimator* Jupyter Notebook
    - Complete skillsets 4, 5 and 6

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Backward Engineer *demo.py* to create *p1_data_analysis_1*
    - Install pandas, pandas-datareader, and matplotlib
    - Create at least three functions: main(), get_requirements(), and data_analysis_1()
    - Create *p1_data_analysis_1.* Jupyter Notebook
    - Complete skillsets 7, 8, and 9

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Backward Engineer *demo.py* and *demo2.py* to create *a4_data_analysis_2*
    - Ensure all necessary packages are installed correctly
    - Create at least three functions: main(), get_requirements(), and data_analysis_2()
    - Create *a4_data_analysis_2* Jupyter Notebook
    - Complete skillsets 10, 11, and 12

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete Introduction to R Setup and Tutorial
    - Code and run lis4369_a5.R
    - Complete skillsets 13, 14, and 15

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backward-engineer project requirements to create lis4369_p2.R
    - Run lis4369_p2.R and include required screenshots
