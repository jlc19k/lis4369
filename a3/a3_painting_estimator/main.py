import functions as f

# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def main():
    # determinant for while loop
    loop = "y"
    
    # only print requirements once
    f.get_requirements()

    while (loop == "y"):
        f.estimate_painting_cost()

        loop = input("\nEstimate another paint job? (y/n): ")

    # print exit message
    print("\nThank you for using our Painting Estimator!"
    + "\nPlease see our web site: http://www.mysite.com")

if __name__ == "__main__":
    main()