# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def get_requirements():
    print("Painting Estimator")
    print("\nProgram Requirements:\n"
    + "1. Calculate home interior paint cost (w/o primer).\n"
    + "2. Must use float data types.\n"
    + "3. Must use SQFT_PER_GALLON constant (350).\n"
    + "4. Must use iteration structure (aka \"loop\").\n"
    + "5. Format, right-align numbers, and round to two decimal places.\n"
    + "6. Create at least five functions that are called by the program:\n"
    + "\ta. main(): calls two other functions: get_requirements() and estimate painting cost().\n"
    + "\tb. get_requirements(): displays the program requirements.\n"
    + "\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
    + "\td. print_painting_estimate(): displays painting costs.\n"
    + "\te. print_painting_percentage(): displays painting costs percentages")
        
def estimate_painting_cost():
    # calculates interior home painting, and calls print functions.
    
    # constant to represent square feet per gallon
    SQFT_PER_GALLON = 350
    
    # IPO
    # get user data
    print("\nInput:")
    sqft = float(input("Enter total interior sq ft: "))
    price_per_gallon = float(input("Enter price per gallon paint: "))
    hourly_rate_per_sqft = float(input("Enter hourly painting rate per sq ft: "))
    
    # Process:
    # calculations
    number_of_gallons = sqft / SQFT_PER_GALLON
    cost_of_paint = number_of_gallons * price_per_gallon
    cost_of_labor = sqft * hourly_rate_per_sqft
    total_cost = cost_of_paint + cost_of_labor
    paint_cost_percentage = (cost_of_paint / total_cost)
    labor_cost_percentage = (cost_of_labor / total_cost)
    
    #display painting costs
    print_painting_estimate(SQFT_PER_GALLON, sqft, price_per_gallon, hourly_rate_per_sqft, number_of_gallons)
    print_painting_percentage(cost_of_paint, cost_of_labor, total_cost, paint_cost_percentage, labor_cost_percentage)
    
    
def print_painting_estimate(SQFT_PER_GALLON, sqft, price_per_gallon, hourly_rate_per_sqft, number_of_gallons):
    # displays painting costs.
    
    print("\nOutput:")
    print("{0:<20} {1:>10}".format("Item", "Amount"))
    print("{0:<20} {1:>10,.2f}".format("Total Sq Ft:", sqft))
    print("{0:<20} {1:>10,.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{0:<20} {1:>10,.2f}".format("Number of Gallons:", number_of_gallons))
    print("{0:<20} ${1:>9,.2f}".format("Paint per Gallon:", price_per_gallon))
    print("{0:<20} ${1:>9,.2f}".format("Labor per Sq Ft:", hourly_rate_per_sqft))


def print_painting_percentage(cost_of_paint, cost_of_labor, total_cost, paint_cost_percentage, labor_cost_percentage):
    print("{0:<10} {1:>10} {2:>12}".format("\nCost", "Amount", "Percentage"))
    print("{0:<10} ${1:>8,.2f} {2:>12,.2%}".format("Paint:", cost_of_paint, paint_cost_percentage))
    print("{0:<10} ${1:>8,.2f} {2:>12,.2%}".format("Labor:", cost_of_labor, labor_cost_percentage))
    print("{0:<10} ${1:>8,.2f} {2:>12}".format("Total:", total_cost, "100.00%"))