> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Assignment 3 Requirements:

1. Backward Engineer *a3_painting_estimator*
2. Organize program into main.py and functions.py
3. Provide screenshots of results
4. Create *a3_painting_estimator* Jupyter Notebook
5. Complete Skillsets 4, 5 and 6

#### README.md file should include the following items:

* Screenshot of a3_painting_estimator application running a loop
* Screenshot of A3 Jupyter Notebook .ipynb file
* Screenshots of Skillsets 4, 5, and 6


#### Assignment Screenshots:

*A3 Painting Estimator*:

![A3 painting estimator](img/a3_vscode.png)

*A3 Jupyter Notebook functions .ipynb*:

![Assignment 3 Jupyter Notebook functions](img/a3_jupyter_1.png)

*A3 Jupyter Notebook main .ipynb*:

![Assignment 3 Jupyter Notebook main](img/a3_jupyter_2.png)

*Skillset 4: Calorie Percentage Screenshot*:

![SS4 Calorie Percentage Screenshot](img/SS4.png)

*Skillset 5: Python Selection Structures Screenshot*:

![SS5 Python Selection Structures Screenshot](img/SS5.png)

*Skillset 6: Python Loops Screenshot*:

![SS6 Python Loops](img/SS6.png)

