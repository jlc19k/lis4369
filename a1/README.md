> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
	a) this assignment and
	b)the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1 .ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb)
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - "initializes" a new git repository.
2. git status - shows the current state of the working directory, including what changes have been made and are awaiting a commit.
3. git add - takes a change in the working directory and adds it to the staging area to be updated during the next commit.
4. git commit - records and pushes changes to files within a repository to the main repository.
5. git push - uploads a local repository and its contents to a remote repository.
6. git pull - obtains content from a remote repositiory 
7. git clone - clones a repository into a newly created directory, initializing it as a new repository. 

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE)*:

![application running in IDLE](img/a1_tip_calculator_idle.png)

*Screenshot of a1_tip_calculator application running (Visual Studio Code)*:

![application running in Visual Studio Code](img/a1_tip_calculator_vs_code.png)

*A1 Jupyter Notebook*:

![Assignment 1 Jupyter Notebook](img/a1_jupyter_notebook.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")
