import os

def get_requirements():
    print("Program Requirements:")
    print("1. Create write_read_file subdirectory with two files: main.py and functions.py.\n"
    +"2. Use President Abraham Lincoln's Gettysburg Address: Full Text.\n"
    +"3. Write address to file.\n"
    +"5. Create Python Docstrings for functions in function.py file.\n"
    +"6. Display Python Docstrings for each function in functions.py file.\n"
    +"7. Display full file path.\n"
    +"8. Replicate display below.\n")

    print("Help on function write_read_file in module functions:")
    print("\nwrite_read_file()\n"
    +"\tUsage: Calls two functions:\n"
    +"\t\t1. file_write() # writes to file\n"
    +"\t\t1. file_read() # reads from file\n"
    +"\tParameters: none\n"
    +"\tReturns: none\n")

    print("Help on function file_write in module functions:\n")
    print("file_write()\n"
    +"\tUsage: creates file, and writes contents of global variable to file\n"
    +"\tParameters: none\n"
    +"\tReturns: none\n")

    print("Help on function file_read in module functions:\n")
    print("file_read()\n"
    +"\tUsage: reads contents of written file\n"
    +"\tParameters: none\n"
    +"\tReturns: none\n")

def write_read_file():
    file_write()
    file_read()

def file_write():
    f = open("test.txt", "w")
    f.write("President Abraham Lincoln's Gettysberg Address:\n"
    +"Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the\n proposition that all men are created equal.\n"
    +"\nNow we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure.\nWe are met on a great battle-field of that war. "
    +"We have come to dedicate a portion of that field, as a final resting place for those who\nhere gave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n"
    +"\nBut, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract.\n"
    +"The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced.\n"
    +"It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion -- \n"
    +"that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth.\n"
    +"\nAbraham Lincoln\nNovember 19, 1863")

    f.write("\n\nFull File Path: ")
    f.write(os.path.realpath(f.name) + "\n")
    f.close()

def file_read():
    f = open("test.txt", "r")
    print(f.read())

