#!/user/bin/env python3
# 2021-09-16 Jason Choate jlc19k@my.fsu.edu
# LIS4369 Jowett - Fall 2021 Semester

import functions as f

def main():
	f.get_requirements()
	f.calculate_it_ict_student_percentage()

if __name__ == "__main__":
	main()