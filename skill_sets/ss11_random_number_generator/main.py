import functions as f

# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def main():

    f.get_requirements()
    print()
    print("Input: ")
    x = int(input("Enter beginning value: "))
    y = int(input("Enter ending value: "))
    y += 1

    print()
    print("Output: ")
    print("Example 1: Using range() and randint() functions: ")
    f.example1(x, y)

    print()
    print("Example 2: Using a list, with range() and shuffle() functions: ")
    f.example2(x, y)

if __name__ == "__main__":
    main()