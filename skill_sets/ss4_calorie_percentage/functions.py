# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def get_requirements():
    print("Calorie Percentage\n")
    print("Program Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calculate percentages.\n"
        + "3. Must use float data types.\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_percent():
    FAT_CAL = 9
    CARB_CAL = 4
    PROTEIN_CAL = 4

    print("Input:")
    fat_grams = float(input("Enter total fat grams: "))
    carb_grams = float(input("Enter total carb grams: "))
    protein_grams = float(input("Enter total protein grams: "))

    # 1 gram of fat = 9 calories
    total_fat = fat_grams * FAT_CAL
    # 1 gram of carbs = 4 calories
    total_carb = carb_grams * CARB_CAL
    # 1 gram of protein = 4 calories
    total_protein = protein_grams * PROTEIN_CAL

    total = total_fat + total_carb + total_protein

    #get percentages
    fat_percent = (total_fat / total)
    carb_percent = (total_carb / total)
    protein_percent = (total_protein / total)

    print("\nOutput:")
    print("{0:<12} {1:>12} {2:>12}".format("Type", "Calories", "Percentage"))
    print("{0:<12} {1:>12,.2f} {2:>12,.2%}".format("Fat", total_fat, fat_percent))
    print("{0:<12} {1:>12,.2f} {2:>12,.2%}".format("Carbs", total_carb, carb_percent))
    print("{0:<12} {1:>12,.2f} {2:>12,.2%}".format("Protein", total_protein, protein_percent))