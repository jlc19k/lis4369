import functions as f

# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def main():
    f.get_requirements()
    list_size = f.user_input()
    f.using_lists(list_size)

if __name__ == "__main__":
    main()