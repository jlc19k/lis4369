def get_requirements():
    print("Python Calculator with Error Handling\n")
    print("Program Requirements:\n"
        +"1. Program calculates two numbers, and rounds to two decimal places.\n"
        +"2. Prompt user for two numbers, and a suitable operator.\n"
        +"3. Use Python error handling to validate data.\n"
        +"4. Test for correct arithmetic operator.\n"
        +"5. Division by zero not permitted.\n"
        +"6. Note: Program loops until correct input entered - numbers and arithmetic operator."
        +"7. Replicate display below."
    )

def calculate():
    num1 = 0
    num2 = 0

    while True:
        num1 = input("\nEnter num1: ")
        if num1.isnumeric() == False:
            print("Not valid number!")
            continue
        break

    while True:
        num2 = input("\nEnter num2: ")
        if num2.isnumeric() == False:
            print("Not valid number!")
            continue
        break

    print("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")

    while True:
        output = 0
        operator = input("Enter operator: ")
        
        if operator == '+':
            output = int(num1) + int(num2)
            break

        elif operator == '-':
            output = int(num1) - int(num2)
            break

        elif operator == '*':
            output = int(num1) * int(num2)
            break

        elif operator == '/' and num2 == '0':
            print("Cannot divide by zero!")
            
            while True:
                num2 = input("\nEnter num2: ")
                if num2.isnumeric() == False or num2 == '0':
                    print("Not valid number!")
                    continue
                break

            output = int(num1) / int(num2)
            break

        elif operator == '/':
            output = int(num1) / int(num2)
            break
        
        elif operator == '//':
            output = int(num1) // int(num2)
            break

        elif operator == '%':
            output = int(num1) % int(num2)
            break

        elif operator == '**':
            output = int(num1) ** int(num2)
            break

        else:
            print("\nIncorrect operator!")

    print("{:0.2f}".format(output))
    print("\nThank you for using our Math Caclulator!")