#functions not function
import functions as f

def main():
    #both functions were spelt wrong
    f.get_requirements()
    f.calculate_payroll()

#__name__ missed two underscores
#__main__ missed three underscores
if __name__ == "__main__":
    main()