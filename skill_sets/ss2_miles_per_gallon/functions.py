#!/user/bin/env python3
# 2021-09-16 Jason Choate jlc19k@my.fsu.edu
# LIS4369 Jowett - Fall 2021 Semester

def print_requirements():
	print("Miles Per Gallon")
	print("Developer: Jason Choate\n")
	print("Program Requirements:\n"
		+ "1. Calculate MPG.\n"
		+ "2. Must use float data type for user input and calculation.\n"
		+ "3. Format and round conversion to two decimal places.\n")
		
def calc_mpg():
	print("Input:")
	miles = float(input("Enter miles driven: "))
	gal = float(input("Enter gallons of fuel used: "))
	
	mpg = miles / gal
	
	print("\nOutput:")
	print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f}".format(miles, "miles driven and", gal, "gallons used = ", mpg))