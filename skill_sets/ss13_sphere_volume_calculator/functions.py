import math

def get_requirements():
    print("Sphere Volume Program\n")
    print("Program Requirements:\n"
        +"1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches,\nand rounds to two decimal places.\n"
        +"2. Must use Python's *built-in* PI and pow() capabilites.\n"
        +"3. Program checks for non-integers and non-numeric values.\n"
        +"4. Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.\n"
    )

def calculate_volume():
    print("Input: ")
    choice = str(input("Do you want to calculate a sphere volume (y or n)? "))
    print("\nOutput: ")
    
    while True:
        while (choice.lower() == 'y'):
            diameter = input("Please enter diameter in inches (integers only): ")
            if (diameter.isnumeric()):
                diameter2 = int(diameter)
                radius = diameter2/2

                volume = 4/3*math.pi*(pow(radius,3))
                gallons = volume/231

                finalvol = str(round(gallons, 2))
                print("\nSphere volume: " + finalvol + " liquid U.S. gallons.\n")
                choice = str(input("Do you want to calculate another sphere volume (y or n)? "))
            else:
                print("\nNot valid integer!")
        else:
            print("\nThank you for using our Sphere Volume Calculator!\n")
            break