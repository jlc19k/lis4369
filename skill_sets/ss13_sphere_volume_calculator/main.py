import functions as f

# Developer: Jason Choate
# Course: LIS4369
# Semester: Fall 2021

def main():

    f.get_requirements()
    f.calculate_volume()


if __name__ == "__main__":
    main()