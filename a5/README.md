> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Jason Choate

### Assignment 5 Requirements:

1. Complete Introduction to R Setup and Tutorial
2. Code and run lis4369_a5.R
3. Complete skillsets 13, 14, and 15

#### README.md file should include the following items:

* At least two 4-panel RStudio screenshots (learn_to_use_r.R and lis4369_a5.R) 
* Screenshots of at least four plots (two from tutorial, two for assignment)
* Screenshots of Skillsets 13, 14, and 15


#### Assignment Screenshots:

*4-panel RStudio screenshot for learn_to_use_r.R*:

![4-panel of learn_to_use_r.R](img/tutorial_rstudio.png)

*4-panel RStudio screenshot for lis4369_a5.R*:

![4-panel of lis4369_a5.R](img/assignment5_rstudio.png)

*Screenshots of two learn_to_use_r.R graphs*:

| ![Graph 1](img/tutorial_graph_1.png) | ![Graph 2](img/tutorial_graph_2.png) |

*Screenshots of two lis4369_a5.R graphs*:

| ![Graph 1](img/assignment_graph_1.png) | ![Graph 2](img/assignment_graph_2.png) |

*Screenshots of skillsets 13, 14 and 15*:

| ![Skillset 13](img/ss13.png) | ![Skillset 14](img/ss14.png) | ![Skillset 15](img/ss15.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
